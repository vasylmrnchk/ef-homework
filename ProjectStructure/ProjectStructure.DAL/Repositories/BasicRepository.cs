﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.Repositories
{
    public abstract class BasicRepository<T> : IBasicRepository<T> where T : Entity
    {
        protected readonly ProjectStructureDbContext _context;
        public BasicRepository(ProjectStructureDbContext context)
        {
            _context = context;
        }

        public IEnumerable<T> ReadAll()
        {
            return _context.Set<T>().AsNoTracking();
        }

        public virtual  T Create(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public T Read(int id)
        {
            return _context.Set<T>().AsNoTracking()
                                    .Where( item => item.Id  == id)
                                    .FirstOrDefault() ?? throw new KeyNotFoundException($"{typeof(T).Name} not found.");
        }

        public virtual void Update(T entity)
        {
            var edited = Read(entity.Id);
            entity.CreatedAt = edited.CreatedAt;
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = Read(id);
            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }
    }
}
