﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.Repositories
{
    public class TasksRepository : BasicRepository<Task>, ITasksRepository
    {
        public TasksRepository(ProjectStructureDbContext context) : base(context)
        {
        }

       public override void Update(Task entity)
        {
            var edited = Read(entity.Id);
            entity.CreatedAt = edited.CreatedAt;
            entity.PerformerId = edited.PerformerId;
            entity.ProjectId = edited.ProjectId;
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
