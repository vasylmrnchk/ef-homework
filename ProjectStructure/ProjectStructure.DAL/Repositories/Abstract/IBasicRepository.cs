﻿using ProjectStructure.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Repositories.Abstract
{
    public interface IBasicRepository<T> where T : Entity
    {
        IEnumerable<T> ReadAll();
        T Read(int id);
        T Create(T entity);
        void Update(T entity);
        void Delete(int id);
    }
}
