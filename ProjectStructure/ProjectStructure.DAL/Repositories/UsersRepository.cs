﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;
using System;

namespace ProjectStructure.DAL.Repositories
{
    public class UsersRepository : BasicRepository<User>, IUsersRepository
    {
        public UsersRepository(ProjectStructureDbContext context) : base(context)
        {

        }

        public override User Create(User entity)
        {
            CheckTeam(entity.TeamId);
            _context.Users.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public override void Update(User entity)
        {
            CheckTeam(entity.TeamId);
            base.Update(entity);
        }

        private void CheckTeam(int? id)
        {
            if (id.HasValue == true  && _context.Set<Team>().Find(id.GetValueOrDefault()) is null)
                throw new ArgumentException("TeamId");
        }
    }
}
