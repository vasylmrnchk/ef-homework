﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;
using System;

namespace ProjectStructure.DAL.Repositories
{
    public class ProjectsRepository : BasicRepository<Project>, IProjectsRepository
    {
        public ProjectsRepository(ProjectStructureDbContext context) : base(context)
        {

        }

        public override Project Create(Project entity)
        {
            CheckAuthorAndTeam(entity);
            return base.Create(entity);
        }

        public override void Update(Project entity)
        {
            CheckAuthorAndTeam(entity);
            base.Update(entity);
        }

        private void CheckAuthorAndTeam(Project entity)
        {
            if (_context.Set<Team>().Find(entity.TeamId) is null || _context.Set<User>().Find(entity.AuthorId) is null)
                throw new ArgumentException("Invalid AuthorId or TeamId");
        }
    }
}
