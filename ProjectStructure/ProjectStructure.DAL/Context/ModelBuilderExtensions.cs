﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>()
            .Property(t => t.Name)
            .HasMaxLength(100);

            modelBuilder.Entity<Project>()
                        .Property(p => p.Name)
                        .HasMaxLength(100);

            modelBuilder.Entity<Project>()
                        .Property(p => p.Description)
                        .HasMaxLength(300);

            modelBuilder.Entity<Project>()
                        .Property(p => p.Deadline)
                        .IsRequired();
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            int index = 1;
            var teams = new List<Team>
            {
                new Team { Id = index++, Name = "Gang of Four"},
                new Team { Id = index++, Name = "Justice League"},
                new Team { Id = index++, Name = "Reservoir Dogs"}
            };

            index = 1;
            var users = new List<User>
            {
                new User { Id = index++, LastName = "Mr. White", FirstName= "Larry", Email="mrwhite@example.com", TeamId= 3, BirthDay= new DateTime(1963, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Mr. Orange", FirstName= "Freddy", Email="mrorange@example.com", TeamId= 3, BirthDay= new DateTime(1964, 3, 9, 16, 5, 7, 123) },
                new User { Id = index++, LastName = "Mr. Blonde", FirstName= "Vic", Email="mrblonde@example.com", TeamId= 3, BirthDay= new DateTime(1955, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Mr. Pink", FirstName= "Steve", Email="mrpink@example.com", TeamId= 3, BirthDay= new DateTime(1971, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Gamma", FirstName= "Erich", Email="gamma@example.com", TeamId= 1, BirthDay= new DateTime(1967, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Helm", FirstName= "Richard", Email="helm@example.com", TeamId= 1, BirthDay= new DateTime(1955, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Johnson", FirstName= "Ralph", Email="johson@example.com", TeamId= 1, BirthDay= new DateTime(1945, 3, 9, 16, 5, 7, 123) },
                new User { Id = index++, LastName = "Vlissides", FirstName= "John", Email="Vlissides@example.com", TeamId= 1, BirthDay= new DateTime(1965, 3, 9, 16, 5, 7, 123) },
                new User { Id = index++, LastName = "Batman", FirstName= "Bruse", Email="batman@example.com", TeamId= 2, BirthDay= new DateTime(1929, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Wonder Woman", FirstName= "Diana", Email="diana@example.com", TeamId= 2, BirthDay= new DateTime(1918, 3, 9, 16, 5, 7, 123) }
            };

            index = 1;
            var projects = new List<Project> 
            { 
                new Project {Id = index++, AuthorId = 1, TeamId = 3, Name = "A diamond heist", Deadline= new DateTime(1992, 3, 9, 16, 5, 7, 123), Description = "Blonde meets with the Cabots, having completed a four."},
                new Project {Id = index++, AuthorId = 5, TeamId = 1, Name = "GoF book", Deadline= new DateTime(1994, 3, 9, 16, 5, 7, 123), Description = "Blonde meets with the Cabots, having completed a four-year jail sentence. To reward him for concealing Joe's name from the"},
                new Project {Id = index++, AuthorId = 9, TeamId = 2, Name = "Batman vs Supermanan", Deadline= new DateTime(2014, 3, 9, 16, 5, 7, 123), Description= "Superman has become a controversial figure. Bruce is now a billionaire who has operated in Gotham City as the vigilante Batman for twenty "}
            };

            index = 1;
            var tasks = new List<Task>
            {
                new Task {Id = index++, ProjectId= 1, PerformerId=3, Name = "A", Description="fadsd"},
                new Task {Id = index++, ProjectId= 1, PerformerId=1, Name = "DSDSf", Description="sdfsdd"},
                new Task {Id = index++, ProjectId= 2, PerformerId=5, Name = "veswgwre", Description="fdsgfrw"},
                new Task {Id = index++, ProjectId= 3, PerformerId=9, Name = "grtwetgrwe", Description="adferge"},
                new Task {Id = index++, ProjectId= 3, PerformerId=10, Name = "dsfgfds", Description="erfgerf"},
                new Task {Id = index++, ProjectId= 1, PerformerId=9, Name = "afg", Description="efghtrtyhj"},
            };

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }
    }
}
