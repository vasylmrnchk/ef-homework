﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.DAL.Entities
{
    public class Project : Entity
    {
        [Required]
        [ForeignKey(nameof(User))]
        public int AuthorId { get; set; }
        public User Author { get; set; }

        [Required]
        [ForeignKey(nameof(Team))]
        public int TeamId { get; set; }
        public Team Team { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public List<Task> Tasks { get; set; }
    }
}
