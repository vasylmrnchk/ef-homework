﻿using System;

namespace ProjectStructure.DAL.Entities
{
    public class Entity
    {
        private DateTime _createdAt;

        public Entity()
        {
            CreatedAt = DateTime.Now;
        }

        public int Id { get; set; }
        public DateTime CreatedAt
        {
            get => _createdAt;
            set => _createdAt = (value == DateTime.MinValue) ? DateTime.Now : value;
        }
    }
}
