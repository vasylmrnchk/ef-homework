﻿using System.Collections.Generic;

namespace ProjectStructure.DAL.Entities
{
    public class Team : Entity
    {
        public string Name { get; set; }

        public List<User> Users { get; set; }
    }
}
