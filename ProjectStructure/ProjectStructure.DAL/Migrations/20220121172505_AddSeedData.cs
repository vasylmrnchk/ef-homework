﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2022, 1, 21, 19, 25, 3, 636, DateTimeKind.Local).AddTicks(3046), "Gang of Four" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 2, new DateTime(2022, 1, 21, 19, 25, 3, 649, DateTimeKind.Local).AddTicks(6143), "Justice League" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 3, new DateTime(2022, 1, 21, 19, 25, 3, 649, DateTimeKind.Local).AddTicks(6291), "Reservoir Dogs" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[,]
                {
                    { 5, new DateTime(1967, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(3963), "gamma@example.com", "Erich", "Gamma", 1 },
                    { 6, new DateTime(1955, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(3997), "helm@example.com", "Richard", "Helm", 1 },
                    { 7, new DateTime(1945, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(4020), "johson@example.com", "Ralph", "Johnson", 1 },
                    { 8, new DateTime(1965, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(4043), "Vlissides@example.com", "John", "Vlissides", 1 },
                    { 9, new DateTime(1929, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(4065), "batman@example.com", "Bruse", "Batman", 2 },
                    { 10, new DateTime(1918, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(4090), "diana@example.com", "Diana", "Wonder Woman", 2 },
                    { 1, new DateTime(1963, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 649, DateTimeKind.Local).AddTicks(8005), "mrwhite@example.com", "Larry", "Mr. White", 3 },
                    { 2, new DateTime(1964, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(3849), "mrorange@example.com", "Freddy", "Mr. Orange", 3 },
                    { 3, new DateTime(1955, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(3914), "mrblonde@example.com", "Vic", "Mr. Blonde", 3 },
                    { 4, new DateTime(1971, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(3939), "mrpink@example.com", "Steve", "Mr. Pink", 3 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, 5, new DateTime(2022, 1, 21, 19, 25, 3, 651, DateTimeKind.Local).AddTicks(839), new DateTime(1994, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), "Blonde meets with the Cabots, having completed a four-year jail sentence. To reward him for concealing Joe's name from the", "GoF book", 1 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, 9, new DateTime(2022, 1, 21, 19, 25, 3, 651, DateTimeKind.Local).AddTicks(907), new DateTime(2014, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), "Superman has become a controversial figure. Bruce is now a billionaire who has operated in Gotham City as the vigilante Batman for twenty ", "Batman vs Supermanan", 2 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2022, 1, 21, 19, 25, 3, 650, DateTimeKind.Local).AddTicks(5498), new DateTime(1992, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), "Blonde meets with the Cabots, having completed a four.", "A diamond heist", 3 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 3, new DateTime(2022, 1, 21, 19, 25, 3, 651, DateTimeKind.Local).AddTicks(7165), "fdsgfrw", null, "veswgwre", 5, 2, 0 },
                    { 4, new DateTime(2022, 1, 21, 19, 25, 3, 651, DateTimeKind.Local).AddTicks(7188), "adferge", null, "grtwetgrwe", 9, 3, 0 },
                    { 5, new DateTime(2022, 1, 21, 19, 25, 3, 651, DateTimeKind.Local).AddTicks(7211), "erfgerf", null, "dsfgfds", 10, 3, 0 },
                    { 1, new DateTime(2022, 1, 21, 19, 25, 3, 651, DateTimeKind.Local).AddTicks(2966), "fadsd", null, "A", 3, 1, 0 },
                    { 2, new DateTime(2022, 1, 21, 19, 25, 3, 651, DateTimeKind.Local).AddTicks(7113), "sdfsdd", null, "DSDSf", 1, 1, 0 },
                    { 6, new DateTime(2022, 1, 21, 19, 25, 3, 651, DateTimeKind.Local).AddTicks(7241), "efghtrtyhj", null, "afg", 9, 1, 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
