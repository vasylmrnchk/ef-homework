﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO.Task;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class TasksController : ControllerBase
    {
        readonly ITasksService _service;
        public TasksController(ITasksService service)
        {
            _service = service;
        }

        // GET: api/<TasksController>
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            return Ok(_service.ReadAll());
        }

        // GET api/<TasksController>/5
        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            return Ok(_service.Read(id));
        }

        // POST api/<TasksController>
        [HttpPost]
        public ActionResult<TaskDTO> Post([FromBody] NewTaskDTO newTaskDTO)
        {
            var task = _service.Create(newTaskDTO);
            return CreatedAtAction(nameof(Get), new { id = task.Id }, task);
        }

        // PUT api/<TasksController>
        [HttpPut]
        public ActionResult Put([FromBody] UpdateTaskDTO updateTaskDTO)
        {
            _service.Update(updateTaskDTO);
            return Ok();
        }

        // DELETE api/<TasksController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _service.Delete(id);
            return NoContent();
        }
    }
}
