﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL;
using ProjectStructure.BLL.DTO.Team;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class TeamsController : ControllerBase
    {
        readonly ITeamsService _service;
        public TeamsController(ITeamsService service)
        {
            _service = service;
        }

        // GET: api/<TeamsController>
        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> Get()
        {
            return Ok(_service.ReadAll());
        }

        // GET api/<TeamsController>/5
        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            return Ok(_service.Read(id));
        }

        // POST api/<TeamsController>
        [HttpPost]
        public ActionResult<TeamDTO> Post([FromBody] NewTeamDTO newTeamDTO)
        {
            var team = _service.Create(newTeamDTO);
            return CreatedAtAction(nameof(Get), new { id = team.Id }, team);
        }

        // PUT api/<TeamsController>
        [HttpPut]
        public ActionResult Put([FromBody] UpdateTeamDTO updateTeamDTO)
        {
            _service.Update(updateTeamDTO);
            return Ok();
        }

        // DELETE api/<TeamsController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _service.Delete(id);
            return NoContent();
        }
    }
}
