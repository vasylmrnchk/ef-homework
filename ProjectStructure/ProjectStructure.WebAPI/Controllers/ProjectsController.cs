﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO.Project;
using ProjectStructure.BLL.Services.Abstract;
using System;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProjectsController : ControllerBase
    {
        readonly IProjectsService _service;
        public ProjectsController(IProjectsService service)
        {
            _service = service;
        }

        // GET: api/<ProjectsController>
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            return Ok(_service.ReadAll());
        }

        // GET api/<ProjectsController>/5
        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            return Ok(_service.Read(id));
        }

        // POST api/<ProjectsController>
        [HttpPost]
        public ActionResult<ProjectDTO> Post([FromBody] NewProjectDTO newProjectDTO)
        {
            var project = _service.Create(newProjectDTO);
            return CreatedAtAction(nameof(Get), new { id = project.Id }, project);
        }

        // PUT api/<ProjectsController>
        [HttpPut]
        public ActionResult Put([FromBody] UpdateProjectDTO updateProjectDTO)
        {
            _service.Update(updateProjectDTO);
            return Ok();
        }

        // DELETE api/<ProjectsController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _service.Delete(id);
            return NoContent();
        }
    }
}
