﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO.User;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _service;
        public UsersController(IUsersService service)
        {
            _service = service;
        }

        // GET: api/<UsersController>
        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> Get()
        {
            return Ok(_service.ReadAll());
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            return Ok(_service.Read(id));
        }

        // POST api/<UsersController>
        [HttpPost]
        public ActionResult<UserDTO> Post([FromBody] NewUserDTO newUserDTO)
        {
            var user = _service.Create(newUserDTO);
            return CreatedAtAction(nameof(Get), new { id = user.Id }, user);
        }

        // PUT api/<UsersController>
        [HttpPut]
        public ActionResult Put([FromBody] UpdateUserDTO updateUserDTO)
        {
            _service.Update(updateUserDTO);
            return Ok();
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _service.Delete(id);
            return NoContent();
        }
    }
}
