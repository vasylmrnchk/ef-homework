﻿using AutoMapper;
using ProjectStructure.BLL.DTO.Project;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<NewProjectDTO, Project>();
            CreateMap<UpdateProjectDTO, Project>();
        }
    }
}
