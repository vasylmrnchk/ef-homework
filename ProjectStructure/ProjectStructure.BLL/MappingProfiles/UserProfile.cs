﻿using AutoMapper;
using ProjectStructure.BLL.DTO.User;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.RegisteredAt, src => src.MapFrom(s => s.CreatedAt));
            CreateMap<NewUserDTO, User>();
            CreateMap<UpdateUserDTO, User>();
        }
    }
}
