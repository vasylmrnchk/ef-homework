﻿using System;

namespace ProjectStructure.BLL.DTO.Task
{
    public class UpdateTaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskStateDTO State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
