﻿using System.Collections.Generic;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface IBaseService<T, TDto, TNewDto, TUpdateDTo>
    {
        IEnumerable<TDto> ReadAll();
        TDto Read(int id);
        TDto Create(TNewDto entityDto);
        void Update(TUpdateDTo entityDto);
        void Delete(int id);
    }
}
