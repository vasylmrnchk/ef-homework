﻿using AutoMapper;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;
using System.Collections.Generic;
using System.Linq;


namespace ProjectStructure.BLL.Services
{
    public class BaseService<T, TDto,TNewDto, TUpdateDto> : IBaseService<T, TDto,TNewDto, TUpdateDto> where T : Entity
    {
        protected readonly IBasicRepository<T> _repository;
        protected readonly IMapper _mapper;
        public BaseService(IBasicRepository<T> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public IEnumerable<TDto> ReadAll()
        {
            return _mapper.Map<IEnumerable<T>, IEnumerable<TDto>>(_repository.ReadAll());
        }

        public TDto Create(TNewDto entityDto)
        {
            var entity = _mapper.Map<T>(entityDto);
            return _mapper.Map<TDto>(_repository.Create(entity));
        }

        public TDto Read(int id)
        {
            return _mapper.Map<TDto>(_repository.Read(id));
        }

        public void Update(TUpdateDto entityDto)
        {
            var entity = _mapper.Map<T>(entityDto);
            _repository.Update(entity);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

    }
}
