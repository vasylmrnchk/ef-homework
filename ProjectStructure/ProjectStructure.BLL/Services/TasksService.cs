﻿using AutoMapper;
using ProjectStructure.BLL.DTO.Task;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;

namespace ProjectStructure.BLL.Services
{
    public class TasksService : BaseService<Task, TaskDTO, NewTaskDTO, UpdateTaskDTO>, ITasksService
    {
        public TasksService(ITasksRepository repository, IMapper mapper) : base(repository, mapper)
        {

        }
    }
}
