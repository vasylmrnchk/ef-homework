﻿using AutoMapper;
using ProjectStructure.BLL.DTO.Team;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;

namespace ProjectStructure.BLL.Services
{
    public class TeamsService : BaseService<Team, TeamDTO,  NewTeamDTO, UpdateTeamDTO>, ITeamsService
    {
        public TeamsService(ITeamsRepository repository, IMapper mapper) : base(repository, mapper)
        {

        }
    }
}
