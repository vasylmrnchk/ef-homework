﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLinqHomework
{
    class ClientService
    {
        readonly private HttpClient _client;
        readonly private string _baseAdress = "https://localhost:5001/api/";
        readonly private string _contentType = "application/json";
        private string _endpoint;

        public string Endpoint { get => _endpoint; set => _endpoint = value; }

        public ClientService()
        {
            _client = new();
            _client.BaseAddress = new Uri(_baseAdress);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue(_contentType));
        }

        public ClientService(string endpoint) : base()
        {
            _endpoint = endpoint;
        }

        //GET
        public async Task<List<T>> GetAllAsync<T>()
        {
            var response = await _client.GetAsync(_endpoint);
            await CheckResponse(response);
            return JsonConvert.DeserializeObject<List<T>>(await response.Content.ReadAsStringAsync());
        }

        //GET
        public async Task<T> GetAsync<T>(int id)
        {
            var response = await _client.GetAsync($"{_endpoint}/{id}");
            await CheckResponse(response);
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        //POST
        public async Task<T> CreateAsync<T, TNew>(TNew entity)
        {
            var content = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, _contentType);

            var response = await _client.PostAsync(_endpoint, content);
            await CheckResponse(response);
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync()); 
        }
   
        //PUT 
        public async Task UpdateAsync<TUpdate>(TUpdate entity)
        {
            var content = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, _contentType);

            var response = await _client.PutAsync(_endpoint, content);
            await CheckResponse(response);
        }

        //DELETE 
        public async Task<HttpStatusCode> RemoveAsync(int id)
        {
            var response = await _client.DeleteAsync($"{_endpoint}/{id}");
            await CheckResponse(response);
            return response.StatusCode;
        }

        /// <summary>
        /// This method check response status code and throw exception when code not Success 
        /// </summary>
        /// <param name="response">HttpResponseMessage instance</param>
        /// <returns></returns>
        private static async Task CheckResponse(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                var message = await response.Content.ReadAsStringAsync();
                throw new Exception(JsonConvert.DeserializeObject<string>(message));
            }
        }
    }
}