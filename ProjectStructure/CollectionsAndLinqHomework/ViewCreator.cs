﻿using CollectionsAndLinqHomework.Models;
using CollectionsAndLinqHomework.Models.Project;
using CollectionsAndLinqHomework.Models.Task;
using CollectionsAndLinqHomework.Models.Team;
using CollectionsAndLinqHomework.Models.User;
using System;

namespace CollectionsAndLinqHomework
{
    internal static class ViewCreator
    {
        public static string CreateMenuItemView(string header, string body, string footer)
        {
            Console.Clear();
            Console.WriteLine($"{header}");
            Console.WriteLine("_________________________________________________\r\n");
            Console.WriteLine(body);
            Console.WriteLine("_________________________________________________");
            Console.Write($"\r\n{footer}");
            return Console.ReadLine();
        }

        public static int CreateIdForm(string modelName)
        {
            Console.Write($"Please input {modelName} id (numder >= 0): ");
            return Convert.ToInt32(Console.ReadLine());
        }

        public static string CreateStringForm(string property)
        {
            Console.Write($"{property}: ");
            return Console.ReadLine();
        }

        public static NewProject CreateProjectForm()
        {
            Console.Clear();
            return new NewProject()
            {
                AuthorId = CreateIdForm("author"),
                TeamId = CreateIdForm("team"),
                Name = CreateStringForm("Name"),
                Description = CreateStringForm("Description"),
                Deadline = Convert.ToDateTime(CreateStringForm("Dealine(date)"))
            };
        }

        public static UpdateProject CreateUpdateProjectForm()
        {
            Console.Clear();
            return new UpdateProject()
            {
                Id = CreateIdForm("project"),
                AuthorId = CreateIdForm("author"),
                TeamId = CreateIdForm("team"),
                Name = CreateStringForm("Name"),
                Description = CreateStringForm("Description"),
                Deadline = Convert.ToDateTime(CreateStringForm("Dealine(date)"))
            };
        }

        public static NewTask CreateTaskForm()
        {
            Console.Clear();
            return new NewTask()
            {
                PerformerId = CreateIdForm("performer"),
                ProjectId = CreateIdForm("project"),
                Name = CreateStringForm("Name"),
                Description = CreateStringForm("Description")
            };
        }

        public static UpdateTask CreateUpdateTaskForm()
        {
            Console.Clear();
            return new UpdateTask()
            {
                Id = CreateIdForm("task"),
                Name = CreateStringForm("Name"),
                Description = CreateStringForm("Description"),
                State = (TaskState)Convert.ToInt32(CreateStringForm("Choose a one of state (Created = 0,  Started = 1, Completed = 2 Canceled=3)")),
                FinishedAt = Convert.ToDateTime(CreateStringForm("FinishedAt"))
            };
        }

        public static NewTeam CreateTeamForm()
        {
            Console.Clear();
            return new NewTeam()
            {
                Name = CreateStringForm("Name")
            };
        }

        public static UpdateTeam CreateUpdateTeamForm()
        {
            Console.Clear();
            return new UpdateTeam()
            {
                Id = CreateIdForm("team"),
                Name = CreateStringForm("Name")
            };
        }

        public static NewUser CreateUserForm()
        {
            Console.Clear();
            return new NewUser()
            {
                TeamId = CreateIdForm("team"),
                FirstName = CreateStringForm("FirstName"),
                LastName = CreateStringForm("LastName"),
                Email = CreateStringForm("Email"),
                BirthDay = Convert.ToDateTime(CreateStringForm("Birthday date"))
            };
        }

        public static UpdateUser CreateUpdateUserForm()
        {
            Console.Clear();
            return new UpdateUser()
            {
                Id = CreateIdForm("user"),
                TeamId = CreateIdForm("team"),
                FirstName = CreateStringForm("FirstName"),
                LastName = CreateStringForm("LastName"),
                Email = CreateStringForm("Email"),
                BirthDay = Convert.ToDateTime(CreateStringForm("Birthday date"))
            };
        }
    }
}
