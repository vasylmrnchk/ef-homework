﻿using CollectionsAndLinqHomework.Models.Project;
using CollectionsAndLinqHomework.Models.Task;
using CollectionsAndLinqHomework.Models.Team;
using CollectionsAndLinqHomework.Models.User;
using System.Collections.Generic;

namespace CollectionsAndLinqHomework
{
    class BasicCollections
    {
        private readonly ClientService service = new();
        public List<Project> Projects { get; private set; } = new List<Project>();
        public List<Task> Tasks { get; private set; } = new List<Task>();
        public List<Team> Teams { get; private set; } = new List<Team>();
        public List<User> Users { get; private set;} = new List<User>();

        public BasicCollections()
        {
        }

        public void GetData()
        {
            service.Endpoint = "projects";
            Projects = service.GetAllAsync<Project>().Result;

            service.Endpoint = "tasks";
            Tasks = service.GetAllAsync<Task>().Result;

            service.Endpoint = "teams";
            Teams = service.GetAllAsync<Team>().Result;

            service.Endpoint = "users";
            Users = service.GetAllAsync<User>().Result;
        }
    }
}
