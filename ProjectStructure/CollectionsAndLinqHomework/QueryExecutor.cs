﻿using CollectionsAndLinqHomework.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionsAndLinqHomework
{
    class QueryExecutor
    {
        private List<ProjectDTO> _projects = new();
        public BasicCollections BasicCollections { get; set; }
        public QueryExecutor()
        {
        }

        /// <summary>
        /// Creating nested "structure" that represented 
        ///     -Project : ProjectDTO
        ///         --Tasks : TaskDTO
        ///             ---Performer :User
        ///         --Author : User
        ///         --Team : Team
        ///         
        /// </summary>
        /// <param name="basicCollections">Contains collections that getting from API Projects, Users, Tasks and Teams</param>
        /// <returns></returns>
        public void CreateProjectList()
        {
            _projects = (from project in BasicCollections.Projects
                    join team in BasicCollections.Teams
                      on project.TeamId equals team.Id
                    join user in BasicCollections.Users
                      on project.AuthorId equals user.Id
                    select new ProjectDTO
                    {
                        Id = project.Id,
                        Name = project.Name,
                        Description = project.Description,
                        Deadline = project.Deadline,
                        CreatedAt = project.CreatedAt,

                        Tasks = (from ts in BasicCollections.Tasks
                                 join u in BasicCollections.Users
                                   on ts.PerformerId equals u.Id
                                 where ts.ProjectId == project.Id
                                 select new TaskDTO
                                 {
                                     Id = ts.Id,
                                     Name = ts.Name,
                                     Description = ts.Description,
                                     State = ts.State,
                                     CreatedAt = ts.CreatedAt,
                                     FinishedAt = ts.FinishedAt,
                                     Performer = u
                                 }).ToList(),

                        Author = user,
                        Team = team,
                    }).ToList();
        }
        //1 
        public Dictionary<ProjectDTO, int> GetUserTasksQuantityPerProject(int userId)
        {
            return _projects.SelectMany(p => p.Tasks,
                                        (p, t) => new { Project = p, User = t.Performer }) 
                            .Where(u => u.User.Id == userId) // u - it's contain union of Project and Task(Task property) 
                            .GroupBy(u => u.Project).ToDictionary(x => x.Key, x => x.Count());
        }

        //1 method GetUserTasksQuantityPerProject in LINQ Query Syntax 
        public Dictionary<ProjectDTO, int> GetUserTasksQuantityPerProjectQS(int userId)
        {
            return (from project in _projects
                    from task in project.Tasks
                    where task.Performer.Id == userId
                    group task by project into grp 
                    select grp).ToDictionary(x => x.Key, x => x.Count());
        }

        //2
        public List<TaskDTO> GetUserTasksWithTaskNameLongThan45(int userId)
        {
            return _projects.SelectMany(p => p.Tasks,
                                        (p, t) => new { Project = p, Task = t })
                            .Where(u => u.Task.Performer.Id == userId &&
                                        u.Task.Name.Length < 45)
                            .Select(u => u.Task).ToList();
        }

        //2 method GetUserTasksWithTaskNameLongThan45 in LINQ Query Syntax 
        public List<TaskDTO> GetUserTasksWithTaskNameLongThan45QS(int userId)
        {
            return (
                     from project in _projects
                     from task in project.Tasks
                     where (task.Performer.Id == userId && task.Name.Length < 45)
                     select task
                   ).ToList();
        }

        //3
        public List<LiteTaskDTO> GetUserTasksFinishedIn2021(int userId)
        {
            return _projects.SelectMany(p => p.Tasks,
                                        (p, t) => new { Project = p, Task = t })
                            .Where(u => u.Task.Performer.Id == userId &&
                                        u.Task.FinishedAt.GetValueOrDefault().Year == 2021)
                            .Select(u => new LiteTaskDTO { Id = u.Task.Id, Name = u.Task.Name }).ToList();
        }

        //3 method GetUserTasksFinishedIn2021 in LINQ Query Syntax 
        public List<LiteTaskDTO> GetUserTasksFinishedIn2021QS(int userId)
        {
            return (
                     from project in _projects
                     from task in project.Tasks
                     where (task.FinishedAt.GetValueOrDefault().Year == 2021 &&
                            task.Performer.Id == userId)
                     select new LiteTaskDTO { Id = task.Id, Name = task.Name }
                   ).ToList();
        }

        //4
        public List<TeamDTO> GetTeamsWithUsersOlderThan10Years()
        {
            return _projects.SelectMany(p => p.Tasks,
                                        (p, t) => new { Project = p, User = t.Performer })
                            .Where(u => DateTime.Now.Year - u.User.BirthDay.Year > 10 &&
                                        u.User.TeamId == u.Project.Team.Id)
                            .OrderByDescending(u => u.User.RegisteredAt)
                            .GroupBy(u => u.Project.Team, u => u.User)
                            .Select(gr => new TeamDTO
                            {
                                Id = gr.Key.Id,
                                Name = gr.Key.Name,
                                Users = gr.Distinct().ToList()
                            }).ToList();
        }

        //4 method GetTeamsWithUsersOlderThan10Years in LINQ Query Syntax
        public List<TeamDTO> GetTeamsWithUsersOlderThan10YearsQS()
        {
            return (
                     from project in _projects
                     from task in project.Tasks
                     where (DateTime.Now.Year - task.Performer.BirthDay.Year > 10 &&
                            task.Performer.TeamId == project.Team.Id)
                     orderby task.Performer.RegisteredAt descending
                     group task.Performer by project.Team into gr
                     select new TeamDTO
                     {
                         Id = gr.Key.Id,
                         Name = gr.Key.Name,
                         Users = gr.Distinct().ToList()
                     }
                   ).ToList();
        }

        //5
        public List<UserTasksDTO> GetSortedByNameAndTaskNameLengthUsers()
        {
            return _projects.SelectMany(p => p.Tasks,
                                        (p, t) => new { Project = p, Task = t })
                            .OrderByDescending(u => u.Task.Name.Length)
                            .OrderBy(u => u.Task.Performer.FirstName)
                            .GroupBy(u => u.Task.Performer, u => u.Task)
                            .Select(gr => new UserTasksDTO
                            {
                                User = gr.Key,
                                Tasks = gr.ToList()
                            }).ToList();
        }

        //5 method GetSortedByNameAndTaskNameLengthUsers in LINQ Query Syntax
        public List<UserTasksDTO> GetSortedByNameAndTaskNameLengthUsersQS()
        {
            return (
                    from project in _projects
                    from task in project.Tasks
                    orderby task.Name.Length descending
                    orderby task.Performer.FirstName ascending
                    group task by task.Performer into gr
                    select new UserTasksDTO
                    {
                        User = gr.Key,
                        Tasks = gr.ToList()
                    }
                   ).ToList();
        }

        //6
        public UserInfoDTO GetUserInfo(int userId)
        {
            return _projects.SelectMany(p => p.Tasks,
                                         (p, t) => new { Project = p, Task = t })
                             .Where(u => u.Task.Performer.Id == userId)
                             .GroupBy(u => u.Task.Performer)
                             .Select(gr => new UserInfoDTO
                             {
                                 User = gr.Key,
                                 LastProject = gr.OrderByDescending(u => u.Project.CreatedAt).FirstOrDefault().Project,
                                 LastProjectTasksNumber = gr.OrderByDescending(
                                                            u => u.Project.CreatedAt).FirstOrDefault().Project.Tasks.Count,
                                 UnfinishedTasksNumber = gr.Where(u => u.Task.FinishedAt.HasValue == false).Count(),
                                 LongestTask = gr.Where(u => u.Task.FinishedAt.HasValue == true)
                                                 .OrderByDescending(u => u.Task.FinishedAt.GetValueOrDefault() -
                                                                         u.Task.CreatedAt
                                                                   ).FirstOrDefault().Task
                             }).FirstOrDefault();
        }

        // 6 method GetUserInfo in LINQ Query Syntax
        public UserInfoDTO GetUserInfoQS(int userId)
        {
            return (from project in _projects
                    from task in project.Tasks
                    where (task.Performer.Id == userId)
                    group new { Project = project, Task = task } by task.Performer into grp
                    select new UserInfoDTO
                    {
                        User = grp.Key,
                        LastProject = grp.OrderByDescending(u => u.Project.CreatedAt).FirstOrDefault().Project,
                        LastProjectTasksNumber = grp.OrderByDescending(
                                                            u => u.Project.CreatedAt).FirstOrDefault().Project.Tasks.Count,
                        UnfinishedTasksNumber = grp.Where(u => u.Task.FinishedAt.HasValue == false).Count(),
                        LongestTask = (
                                        from item in grp
                                        where item.Task.FinishedAt.HasValue == true
                                        orderby (item.Task.FinishedAt.GetValueOrDefault() - item.Task.CreatedAt) descending
                                        select item
                                      ).FirstOrDefault().Task
                    }).FirstOrDefault();
        }

        //7
        public List<ProjectInfoDTO> GetProjectsInfo()
        {
            return _projects.SelectMany(p => p.Tasks,
                                        (p, t) => new { Project = p, Task = t })
                            .Where(u => u.Project.Description.Length > 20 ||
                                        u.Project.Tasks.Count < 3)
                            .GroupBy(u => u.Project, u => u.Task)
                            .Select(gr => new ProjectInfoDTO
                            {
                                Project = gr.Key,
                                LongestTaskByDescription = gr.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                                ShortestTaskByName = gr.OrderBy(t => t.Name.Length).FirstOrDefault(),
                                UsersNumber = BasicCollections.Users.Where(usr => usr.TeamId == gr.Key.Team.Id).Count()
                            }).ToList();
        }

        //7 method GetProjectInfoQS in LINQ Query Syntax
        public List<ProjectInfoDTO> GetProjecstInfoQS()
        {
            return (
                    from project in _projects
                    from task in project.Tasks
                    where (project.Description.Length > 20 || project.Tasks.Count < 3)
                    group task by project into grp
                    select new ProjectInfoDTO
                    {
                        Project = grp.Key,
                        LongestTaskByDescription = grp.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                        ShortestTaskByName = grp.OrderBy(t => t.Name.Length).FirstOrDefault(),
                        UsersNumber = (
                                            from user in BasicCollections.Users
                                            where user.TeamId == grp.Key.Team.Id
                                            select user
                                         ).Count()
                    }).ToList();
        }
    }
}
