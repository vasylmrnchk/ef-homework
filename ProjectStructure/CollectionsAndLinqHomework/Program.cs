﻿using CollectionsAndLinqHomework.Menus;

namespace CollectionsAndLinqHomework
{
    class Program
    {
        static void Main(string[] args)
        {
            var basicCollections = new BasicCollections();
            //in class QueryExecutor all LINQ queries
            var executor = new QueryExecutor
            {
                BasicCollections = basicCollections
            };
            MenuItemsExecutor.ModelService = new ClientService();
            MenuLinqItemsExecutor.LinqExecutor = executor;

            bool showMenu = true;
            while (showMenu)
            {
                
                showMenu = Menu.Menu.MainMenu();
            }
        }
    }
}
