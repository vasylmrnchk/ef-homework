﻿namespace CollectionsAndLinqHomework.Models.Task
{
    public class NewTask
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
