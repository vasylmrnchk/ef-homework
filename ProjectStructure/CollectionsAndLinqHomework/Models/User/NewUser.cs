﻿using System;

namespace CollectionsAndLinqHomework.Models.User
{
    public class NewUser
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
