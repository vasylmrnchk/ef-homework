﻿using System;

namespace CollectionsAndLinqHomework.Models.User
{
    public class UpdateUser
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}