﻿using System;

namespace CollectionsAndLinqHomework.Menus
{
    internal static class MenuItemsExecutor
    {
        public static ClientService ModelService { get; set; }

        public static string Create<T, TNew>(TNew item)
        {
            try
            {
                var created = ModelService.CreateAsync<T, TNew>(item).Result;
                return $"{typeof(T).Name} \"{created}\" created successfully.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public static string Get<T>(int id)
        {
            try
            {
                return ModelService.GetAsync<T>(id).Result.ToString();
            }
            catch (Exception e) { return e.Message; }

        }

        public static string Update<T, TUpdate>(TUpdate item)
        {
            try
            {
                ModelService.UpdateAsync<TUpdate>(item).Wait();
                return $"{typeof(T).Name} \"{item}\" updated successfully.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Delete<T>(int id)
        {
            try
            {
                ModelService.RemoveAsync(id).Wait();
                return $"{typeof(T).Name} with id={id} removed successfully.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetAll()
        {
            throw new NotImplementedException();
        }
    }
}

