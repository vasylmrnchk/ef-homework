﻿using System;
using static CollectionsAndLinqHomework.ViewCreator;
using static CollectionsAndLinqHomework.Menus.MenuItemsExecutor;
using static CollectionsAndLinqHomework.Menus.MenuLinqItemsExecutor;
using CollectionsAndLinqHomework.Models.Project;
using CollectionsAndLinqHomework.Models.User;
using CollectionsAndLinqHomework.Models.Team;
using CollectionsAndLinqHomework.Models.Task;

namespace CollectionsAndLinqHomework.Menu
{
    internal static class Menu
    {
        readonly static string subMenuFooterText = "Press <Enter> for come back to last menu: ";
        readonly static string menuFooterText = "Select an option: ";
        readonly static string[] mainMenuNames = {
            "1) Quantity of user task(s) in project(s)",
            "2) User task(s) with  task name shorter 45 chars",
            "3) User task(s) finished in 2021 year",
            "4) User(s) in team(s) which are older than 10 year old",
            "5) User(s) sorted by name with task(s) sorted by task name length",
            "6) User info",
            "7) Projects info",
            "8) Exit",
            "===================================",
            "a) Project menu",
            "b) Task menu",
            "c) Team menu",
            "d) User menu"
        };

        readonly static string[] subMenuNames = {
            "1) Create",
            "2) Update",
            "3) Delete",
            "4) Back to main menu"
        };

        internal static bool MainMenu()
        {
            string option = CreateMenuItemView(
                "Main menu", String.Join("\n", mainMenuNames), menuFooterText);

            LinqExecutor.BasicCollections.GetData();
            LinqExecutor.CreateProjectList();
            try
            {
                int id;
                switch (option)
                {
                    case "1":
                        id = CreateIdForm("user");
                        CreateMenuItemView($"User with id ={id} has: ", MenuItem1(id), subMenuFooterText);
                        return true;
                    case "2":
                        id = CreateIdForm("user");
                        CreateMenuItemView("", MenuItem2(id), subMenuFooterText);
                        return true;
                    case "3":
                        id = CreateIdForm("user");
                        CreateMenuItemView("", MenuItem3(id), subMenuFooterText);
                        return true;
                    case "4":
                        CreateMenuItemView($"Users in Teams which are older than 10 year old:", MenuItem4(), subMenuFooterText);
                        return true;
                    case "5":
                        CreateMenuItemView($"Users sorted by name with tasks sorted by task name length descending:", MenuItem5(), subMenuFooterText);
                        return true;
                    case "6":
                        id = CreateIdForm("user");
                        CreateMenuItemView($"Info of user with id={id}:", MenuItem6(id), subMenuFooterText);
                        return true;
                    case "7":
                        CreateMenuItemView($"Projects info:", MenuItem7(), subMenuFooterText);
                        return true;
                    case "8":
                        return false;
                    case "a":
                        while (ProjectMenu())
                        {
                        }
                        return true;
                    case "b":
                        while (TaskMenu())
                        {
                        }
                        return true;
                    case "c":
                        while (TeamMenu())
                        {
                        }
                        return true;
                    case "d":
                        while (UserMenu())
                        {
                        }
                        return true;
                    default:
                        return true;
                }
            }
            catch
            {
                return true;
            }
        }

        private static bool UserMenu()
        {
            string option = CreateMenuItemView($"User menu", String.Join("\n", subMenuNames),
                              menuFooterText);

            ModelService.Endpoint = "users";
            switch (option)
            {
                case "1":
                    CreateMenuItemView("", Create<User, NewUser>(CreateUserForm()), subMenuFooterText);
                    return true;
                case "2":
                    CreateMenuItemView("", Update<User, UpdateUser>(CreateUpdateUserForm()), subMenuFooterText);
                    return true;
                case "3":
                    CreateMenuItemView("", Delete<User>(CreateIdForm("user")), subMenuFooterText);
                    return true;
                case "4":
                    return false;
                default:
                    return true;
            }
        }

        private static bool TeamMenu()
        {
            string option = CreateMenuItemView($"Team menu", String.Join("\n", subMenuNames),
                              menuFooterText);

            ModelService.Endpoint = "teams";
            switch (option)
            {
                case "1":
                    CreateMenuItemView("", Create<Team, NewTeam>(CreateTeamForm()), subMenuFooterText);
                    return true;
                case "2":
                    CreateMenuItemView("", Update<Team, UpdateTeam>(CreateUpdateTeamForm()), subMenuFooterText);
                    return true;
                case "3":
                    CreateMenuItemView("", Delete<Team>(CreateIdForm("team")), subMenuFooterText);
                    return true;
                case "4":
                    return false;
                default:
                    return true;
            }
        }

        private static bool TaskMenu()
        {
            string option = CreateMenuItemView($"Task menu", String.Join("\n", subMenuNames),
                              menuFooterText);

            ModelService.Endpoint = "tasks";
            switch (option)
            {
                case "1":
                    CreateMenuItemView("", Create<Task, NewTask>(CreateTaskForm()), subMenuFooterText);
                    return true;
                case "2":
                    CreateMenuItemView("", Update<Task, UpdateTask>(CreateUpdateTaskForm()), subMenuFooterText);
                    return true;
                case "3":
                    CreateMenuItemView("", Delete<Task>(CreateIdForm("task")), subMenuFooterText);
                    return true;
                case "4":
                    return false;
                default:
                    return true;
            }
        }

        private static bool ProjectMenu()
        {
            string option = CreateMenuItemView($"Project menu", String.Join("\n", subMenuNames),
                              menuFooterText);

            ModelService.Endpoint = "projects";
            switch (option)
            {
                case "1":
                    CreateMenuItemView("", Create<Project, NewProject>(CreateProjectForm()), subMenuFooterText);
                    return true;
                case "2":
                    CreateMenuItemView("", Update<Project, UpdateProject>(CreateUpdateProjectForm()), subMenuFooterText);
                    return true;
                case "3":           
                    CreateMenuItemView("", Delete<Project>(CreateIdForm("project")), subMenuFooterText); 
                    return true;
                case "4":
                    return false;
                default:
                    return true;
            }
        }
    }
}
